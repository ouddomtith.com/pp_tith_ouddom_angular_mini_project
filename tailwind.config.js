/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,ts}", "./node_modules/flowbite/**/*.js"],
  theme: {
    colors: {
      transparent: "transparent",
      current: "currentColor",
      "primaryy": "#1B3764",
      "secound-primary": "#FFCA42 ",
    },
    fontFamily: {
      rubik: ["Rubik", "sans-serif"],
    },
    extend: {},
  },
  plugins: [require("flowbite/plugin"), require("daisyui")],
  daisyui: {
    themes: false, 
    darkTheme: "light", 
    base: true,
    styled: true, 
    utils: true, 
    rtl: false, 
    prefix: "", 
    logs: true, 
  },
};
