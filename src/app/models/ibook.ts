export interface IBook {
    id: number;
    title: string;
    description: string;
    bookImage: string;
    author: string;
    category: string;
}

export const Category=[
    'Business',
    'Science',
    'Fiction',
    'Philosophy',
    'Biography'
]