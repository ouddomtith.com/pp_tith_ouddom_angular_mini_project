import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomepageComponent } from './components/homepage/homepage.component';
import { LoginComponent } from './components/login/login.component';
import { BookpageComponent } from './components/bookpage/bookpage.component';
import { AddBookComponent } from './components/add-book/add-book.component';
import { BookContentComponent } from './components/book-content/book-content.component';
import { authGuard } from './guard/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: HomepageComponent,
  },
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'book',
    component: BookpageComponent,
    canActivate:[authGuard]
  },
  {
    path: 'book/add',
    component: AddBookComponent,
    canActivate:[authGuard]
  },
  {
    path: 'book/:id',
    component: BookContentComponent,
    canActivate:[authGuard]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
