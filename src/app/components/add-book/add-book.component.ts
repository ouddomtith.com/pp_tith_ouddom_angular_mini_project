import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { IBook } from 'src/app/models/ibook';
import { BookService } from 'src/app/services/book.service';

@Component({
	selector: 'app-add-book',
	templateUrl: './add-book.component.html',
	styleUrls: ['./add-book.component.css']
})
export class AddBookComponent {
	formBook!: FormGroup;
	url: any;
	msg = "";
	categories = [
		'Business',
		'Science',
		'Fiction',
		'Philosophy',
		'Biography'
	]
	constructor(private _bookService: BookService, private router: Router) { }

	selectFile(event: any) {
		var reader = new FileReader();
		reader.readAsDataURL(event.target.files[0]);
		reader.onload = (_event) => {
			this.url = reader.result;
			this.msg = "uploaded"
		}
	}

	ngOnInit(): void {
		this.formBook = new FormGroup({
			title: new FormControl(null, Validators.required),
			author: new FormControl(null, Validators.required),
			bookImage: new FormControl(null, Validators.required),
			category: new FormControl(null, Validators.required),
			description: new FormControl(null, Validators.required),
		}
		);
	}


	handleCreateBook() {
		const bookData: IBook = {
			id: 3,
			title: this.formBook.value.title,
			description: this.formBook.value.description,
			bookImage: this.url,
			author: this.formBook.value.author,
			category: this.formBook.value.category
		}
		this._bookService.postBook(bookData).subscribe((res) => {
		})
		this.router.navigate(['/book'])
	}
}
