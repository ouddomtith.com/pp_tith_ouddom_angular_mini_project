import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { IBook } from 'src/app/models/ibook';
import { BookService } from 'src/app/services/book.service';

@Component({
  selector: 'app-bookpage',
  templateUrl: './bookpage.component.html',
  styleUrls: ['./bookpage.component.css'],
})
export class BookpageComponent {
  constructor(private _bookService: BookService, private _router: Router) {}

  books!: IBook[];

  book!: IBook;

  category_list!: string[];

  formEdit!: FormGroup;

  searchCategory: string = '';

  category:string = '';

  searchTerm:string = ''

  ngOnInit(): void {
    this.getBooks();
    this.formEdit = new FormGroup({
      title: new FormControl(null),
      author: new FormControl(null),
      description: new FormControl(null),
    });
    this.category_list = this._bookService.category_list;
  }

  categoryTerm(category:string){
    this.category=category
  }

  getBooks(): void {
    this._bookService.getBooks().subscribe({
      next: (val) => (this.books = val),
      error: (err) => console.log(err),
    });
  }

  deleteBook(book: IBook): void {
    this._bookService.deleteBook(book).subscribe({
      next: (res) => console.log(res),
      error: (err) => console.log(err),
    });
    this.getBooks();
  }

  bookContent(id: number): void {
    for (let book of this.books) {
      if (book.id === id) {
        this._router.navigate(['/book', book.id]);
      }
    }
  }

  edit(): void {
    const book = {
      ...this.formEdit.value,
      id: this.book.id,
      bookImage: this.book.bookImage,
    };
    this._bookService.editBook(book).subscribe({
      next: (res) => console.log(res),
      error: (err) => console.log(err),
    });
    this.getBooks();
  }

  editBook(book: IBook): void {
    this.formEdit.setValue({
      title: book.title,
      author: book.author,
      description: book.description,
    });
    this.book = book;
  }
}
