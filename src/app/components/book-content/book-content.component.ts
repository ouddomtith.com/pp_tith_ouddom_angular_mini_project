import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IBook } from 'src/app/models/ibook';
import { BookService } from 'src/app/services/book.service';

@Component({
  selector: 'app-book-content',
  templateUrl: './book-content.component.html',
  styleUrls: ['./book-content.component.css'],
})
export class BookContentComponent {
  constructor(
    private _bookService: BookService,
    private _activaedRoute: ActivatedRoute
  ) {}

  book!: IBook | undefined;
  id!: number;

  ngOnInit(): void {
    this.getBookFromUrl();
  }

  getBookFromUrl(): void {
    this._activaedRoute.paramMap.subscribe(
      (res) => (this.id = +res.get('id')!)
    );
    this._bookService.getBooks().subscribe({
      next: (res) => (this.book = res.find((val) => val.id === this.id)),
      error: (err) => console.log(err),
    });
  }
}
