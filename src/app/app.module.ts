import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { HomepageComponent } from './components/homepage/homepage.component';
import { BookpageComponent } from './components/bookpage/bookpage.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { BookContentComponent } from './components/book-content/book-content.component';
import { AddBookComponent } from './components/add-book/add-book.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { BookDataService } from './services/book-data.service';
import { HttpClientModule } from '@angular/common/http';
import { SearchCategoryPipe } from './pipes/search-category.pipe';
import { EmailDirective } from './directives/email.directive';
import { PasswordDirective } from './directives/password.directive';
import { SearchBookPipe } from './pipes/search-book.pipe';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomepageComponent,
    BookpageComponent,
    NavbarComponent,
    BookContentComponent,
    AddBookComponent,
    SearchCategoryPipe,
    EmailDirective,
    PasswordDirective,
    SearchBookPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    FormsModule,
    InMemoryWebApiModule.forRoot(BookDataService)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
