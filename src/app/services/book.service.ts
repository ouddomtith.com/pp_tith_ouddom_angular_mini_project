import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IBook } from '../models/ibook';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class BookService {
  private bookUrl: string = 'api/books/';
  category_list: string[] = [
    'Business',
    'Science',
    'Fiction',
    'Philosophy',
    'Biography',
  ];
  constructor(private _httpClient: HttpClient) {}

  getBooks(): Observable<IBook[]> {
    return this._httpClient.get<IBook[]>(this.bookUrl);
  }

  postBook(book: IBook): Observable<IBook> {
    return this._httpClient.post<IBook>(this.bookUrl, book);
  }

  deleteBook(book: IBook): Observable<IBook> {
    return this._httpClient.delete<IBook>(this.bookUrl + book.id);
  }

  editBook(book: IBook): Observable<IBook> {
    return this._httpClient.put<IBook>(this.bookUrl + book.id, book);
  }
}
