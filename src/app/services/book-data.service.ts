import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
@Injectable({
  providedIn: 'root',
})
export class BookDataService implements InMemoryDbService {
  constructor() {}

  createDb() {
    return {
      books: [
        {
          id: 1,
          title: 'Long Brigth River',
          description:
            'Many variations of passages of Lorem Ipsum willing araise alteration in some form.',
          bookImage: '../../../assets/food.jpg',
          author: 'Jonh Abraham',
          category: 'Science',
        },
        {
          id: 2,
          title: 'Long Brigth River',
          description:
            'Many variations of passages of Lorem Ipsum willing araise alteration in some form.',
          bookImage: '../../../assets/food.jpg',
          author: 'Jonh Abraham',
          category: 'Science',
        },
      ],
    };
  }
}
