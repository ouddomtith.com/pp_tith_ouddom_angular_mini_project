import { Directive } from '@angular/core';
import {
  AbstractControl,
  NG_VALIDATORS,
  ValidationErrors,
  Validator,
} from '@angular/forms';

@Directive({
  selector: '[appEmail]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: EmailDirective,
      multi: true,
    },
  ],
})
export class EmailDirective implements Validator {
  constructor() {}

  validate(control: AbstractControl): ValidationErrors | null {
    const regex = /^[A-Za-z]+_\d{3}@gmail\.com$/;
    if (!regex.test(control.value)) {
      return { emailRestricted: true };
    }
    return null;
  }
}
