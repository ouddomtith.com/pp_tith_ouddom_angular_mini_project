import { Directive } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, ValidationErrors, Validator } from '@angular/forms';

@Directive({
  selector: '[appPassword]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: PasswordDirective,
      multi: true,
    },
  ],

})
export class PasswordDirective implements Validator{

  constructor() { }

  validate(control: AbstractControl): ValidationErrors | null {
    const regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,}$/;
    if (!regex.test(control.value)) {
      return { passwordRestricted: true };
    }
    return null;
  }


}
