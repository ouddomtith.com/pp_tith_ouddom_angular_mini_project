import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchCategory',
})
export class SearchCategoryPipe implements PipeTransform {
  transform(books: any[], searchTerm: string): any[] {
    if (!books || !searchTerm) {
      return books;
    }
    
    // Perform the search by filtering the books array
    return books.filter(book => 
      book.category.toLowerCase().includes(searchTerm.toLowerCase()));
  }
}
