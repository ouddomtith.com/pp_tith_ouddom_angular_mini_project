import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchBook'
})
export class SearchBookPipe implements PipeTransform {
  transform(books: any[], searchTerm: string): any[] {
    if (!books || !searchTerm) {
      return books;
    }
    
    // Perform the search by filtering the books array
    return books.filter(book => 
      book.title.toLowerCase().includes(searchTerm.toLowerCase()));
  }
}
